import React, { Component, useState } from "react";
import ProductList from "./Components/ProductList";
import Modal from "./Components/Modal";
import { data } from "./Components/Data";

export default function ShoesStore() {
  // state
  let [products, setProducts] = useState(data);
  let [cart, setCart] = useState([]);
  let [detail, setDetail] = useState({});
  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    // th shoe chưa có trong giỏ hàng -> index=-1
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    }
    // th shoe đã có trong giỏ -> index!=-1 -> tăng số lượng
    else {
      cloneCart[index].soLuong++;
    }
    setCart(cloneCart);
  };
  let handleDelete = (id) => {
    let newCart = cart.filter((item) => {
      return item.id != id;
    });
    setCart(newCart);
  };
  let handleChangeQuantity = (id, soLuong) => {
    let cloneCart = [...cart];
    console.log(id);
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    console.log(index);
    if (index != -1) {
      if (soLuong == -1 && cloneCart[index].soLuong == 1) {
        cloneCart.splice(index, 1);
      } else {
        cloneCart[index].soLuong = cloneCart[index].soLuong + soLuong;
      }
    }
    setCart(cloneCart);
  };
  let handleDetail = (shoe) => {
    let newShoe = { ...shoe };
    setDetail(newShoe);
  };
  return (
    <div>
      <h2>ShoesStore</h2>
      <ProductList
        products={products}
        cart={cart}
        handleAddToCart={handleAddToCart}
        handleDelete={handleDelete}
        handleChangeQuantity={handleChangeQuantity}
        handleDetail={handleDetail}
      ></ProductList>
      {detail != "" && <Modal detail={detail}></Modal>}
    </div>
  );
}
