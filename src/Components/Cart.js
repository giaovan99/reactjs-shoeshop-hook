import React from "react";

export default function Cart({ cart, handleDelete, handleChangeQuantity }) {
  let renderTableCart = () => {
    return cart.map((item) => {
      return (
        <tr>
          <td>
            <img src={item.image} style={{ width: 50 }} />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
            >
              <i class="bi bi-dash"></i>
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              className="btn btn-outline-success"
              onClick={() => {
                handleChangeQuantity(item.id, 1);
              }}
            >
              <i class="bi bi-plus"></i>
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                handleDelete(item.id);
              }}
            >
              <i class="bi bi-trash3-fill"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <div>
        <h3 className="text-info">Your Cart</h3>
        <table className="table table-striped">
          <thead>
            <tr>
              <td>Image</td>
              <td>Name</td>
              <td>Quantity</td>
              <td>Price</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{renderTableCart()}</tbody>
        </table>
      </div>
    </div>
  );
}
